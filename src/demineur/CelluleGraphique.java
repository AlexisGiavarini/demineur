package demineur;

import javax.swing.JButton;

public class CelluleGraphique extends JButton {
  private int x;
  
  private int y;
  
  public CelluleGraphique(String libelle, int x, int y) {
    super(libelle);
    this.x = x;
    this.y = y;
  }
  
  public int donneLigne() {
    return this.x;
  }
  
  public int donneColonne() {
    return this.y;
  }
}

