package demineur;

/**
 * Represente une cellule dans un jeu de demineur.
 *
 */
public class Cellule
{
    private int x,y;        // coordonnees
    private String libelle; // libelle (M pour Mine)
    private int val;        // nombre de mines adjacentes
    private boolean decouverte;   // case decouverte ou non

    /**
     * Cree une cellule de jeu.
     * @param x numero de ligne
     * @param y numero de colonne
     * @param libelle chaine de caractere decrivant le contenu de la cellule
     */
    public Cellule(int x, int y, String libelle)
    {
        this.x = x;
        this.y = y;
        this.libelle = libelle;
        this.val = 0;
        this.decouverte = false;
    }

    // methodes accesseurs...
    public void changeLigne(int ligne)
    {
        this.x = ligne;
    }

    public void changeColonne(int col)
    {
        this.y = col;
    }

    public void changeLibelle(String nouveauLibelle)
    {
        this.libelle = nouveauLibelle;
    }

    public void changeValeur(int val)
    {
        this.val = val;
    }

    public void marque(boolean etat)
    {
        this.decouverte = etat;
    }

    public int donneLigne()
    {
        return this.x;
    }

    public int donneColonne()
    {
        return this.y;
    }

    public String donneLibelle()
    {
        return this.libelle;
    }

    public int donneValeur()
    {
        return this.val;
    }

    public boolean aEteJouee()
    {
        return this.decouverte;
    }

    public String toString()
    {
        String s = this.x + " " + this.y ;
        if (this.libelle.equals("M"))
            s += " M";
        else
            s += " " + (this.decouverte ? "X" : " ");
        return s;
    }
}
