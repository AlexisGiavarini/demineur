package demineur;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Represente l'interface d'un jeu de demineur. A noter que cette classe
 * n'herite pas de JFrame mais contient un objet de ce type.
 *
 */
public class DemineurGraphique {

    private JFrame fenetre;
    private int taille;
    private JButton[][] grille;

    /**
     * Construit l'interface graphique en associant a celle-ci un controleur.
     *
     * @param unCtrl un controleur
     */
    public DemineurGraphique(DemineurControleur unCtrl, int taille) {
        this.taille = taille;
        this.grille = new JButton[this.taille][this.taille];

        this.fenetre = new JFrame("Demineur");
        this.fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridLayout grid = new GridLayout(taille, taille);

        JPanel panel = new JPanel();
        this.fenetre.setContentPane(panel);
        panel.setLayout(grid);

        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                this.grille[i][j] = new CelluleGraphique(" ", i, j);
                this.grille[i][j].addActionListener(unCtrl);
                this.grille[i][j].setPreferredSize(new Dimension(60, 60));
                this.grille[i][j].setBackground(Color.black);
            }
        }

        for (int i = 0; i < this.grille.length; i++) {
            for (int j = 0; j < this.grille.length; j++) {
                panel.add(this.grille[i][j]);
            }
        }
    }

    /**
     * Affiche l'interface graphique une fois que les references entre les trois
     * objets (interface, controleur, jeu) ont ete initialisees.
     */
    public void afficheInterface() {
        this.fenetre.pack();
        this.fenetre.setVisible(true);
    }

    /**
     * Affiche un panneau de dialogue avec un message d'information signalant la
     * cellule venant d'etre selectionnee.
     *
     * @see JOptionPane
     */
    public void afficheAction() {
        JOptionPane.showMessageDialog(this.fenetre,
                "Cellule selectionnee : "
                + "a completer",
                "Action effectuée",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Affiche un panneau de dialogue avec un message d'avertissement signalant
     * que la case selectionnee contient une mine et met fin au jeu.
     *
     * @param x ligne de la case selectionnee
     * @param y colonne de la case selectionnee
     */
    public void afficheEchecEtFin(int x, int y) {
        this.grille[x][y].setText("M");
        this.grille[x][y].setBackground(Color.red);
        JOptionPane.showMessageDialog(this.fenetre, "Mine (" + (x + 1) + "," + (y + 1) + ") ", "Perdu !", 2);
        System.exit(0);
    }

    /**
     * Affiche un panneau de dialogue avec un message d'information signalant
     * que le jeu est terminee (toutes les cases ne contenant pas de mines ont
     * ete decouvertes).
     *
     */
    public void afficheGagneEtFin() {
        JOptionPane.showMessageDialog(this.fenetre, "Félicitations", "Gagné !", 2);
        System.exit(0);
    }

    /**
     * Affiche les cases decouvertes apres la selection d'une case.
     *
     * @param result dictionnaire contenant des couples (numero de case, valeur
     * de la case)
     */
    public void afficheCasesDecouvertes(ArrayList<Cellule> cellules) {
        for (Cellule c : cellules) {
      if (c.donneValeur() != 0)
        this.grille[c.donneLigne()][c.donneColonne()].setText(String.valueOf(c.donneValeur())); 
      if (c.donneLibelle().equals("M")) {
        this.grille[c.donneLigne()][c.donneColonne()].setText(c.donneLibelle());
        this.grille[c.donneLigne()][c.donneColonne()].setBackground(Color.red);
        continue; //Passe de suite à l'iteration suivante
      }
      this.grille[c.donneLigne()][c.donneColonne()].setBackground(Color.white);
    } 
    }
}
