package demineur;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Classe definissant un jeu de demineur. Le but etant de decouvrir l'ensemble
 * des cellules d'une grille qui ne contiennent pas de mines.
 */
public class DemineurJeu
{
    // grille du jeu
    private Cellule[][] grilleJeu;
    // taille de la grille (hypothese d'une grille carree)
    private int tailleGrille;
    // probabilite pour une case de contenir une mine (0 < valeur < 1)
    private double probabiliteMine;

    /**
     * Creer un jeu en precisant la taille de la grille de jeu et la probabilite
     * de presence d'une mine associee a chaque cellule.
     * @param tailleGrille taille de la grille de jeu
     * @param probabilite probabilite de presence d'une mine sur une cellule
     */
    public DemineurJeu(int tailleGrille, double probabilite)
    {
        this.tailleGrille = tailleGrille;
        this.probabiliteMine = probabilite;
        this.grilleJeu = new Cellule[this.tailleGrille][this.tailleGrille];
        // creation de la grille
        for (int i = 0; i < this.tailleGrille; i++)
        {
            for (int j = 0; j < this.tailleGrille; j++)
            {
                // determination aleatoire du contenu de chaque cellule
                if (probabiliteMine < Math.random())
                {
                    this.grilleJeu[i][j] = new Cellule(i, j, " ");
                }
                else
                {
                    this.grilleJeu[i][j] = new Cellule(i, j, "M");
                }
            }
        }
        // calcul des valeurs des cellules
        // chaque cellule indique par une valeur entiere le nombre de mines
        // qui l'entoure
        for (int i = 0; i < this.tailleGrille; i++)
        {
            for (int j = 0; j < this.tailleGrille; j++)
            {
                int valeur = 0;
                for (int dx = -1; dx <= 1; dx++)
                {
                    for (int dy = -1; dy <= 1; dy++)
                    {
                        valeur += this.calculValeurCellule(i + dx, j + dy);
                    }
                }
                this.grilleJeu[i][j].changeValeur(valeur);
            }
        }
    }

    /** Donne la taille du jeu, methode appelee par l'interface graphique pour
     *  determiner la taille de la matrice de boutons a creer
     */
    public int donneTailleJeu()
    {
        return this.tailleGrille;
    }

    /**
     * Indique si la partie est terminee : toutes les cases ne contenant pas
     * de mines ont ete decouvertes.
     * @return true si tel est le cas
     */
    public boolean partieTerminee()
    {
        for (int i = 0; i < this.tailleGrille; i++)
        {
            for (int j = 0; j < this.tailleGrille; j++)
            {
                if (!this.grilleJeu[i][j].aEteJouee()
                        && !this.grilleJeu[i][j].donneLibelle().equals("M"))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Donne l'etat de la grille de jeu via une liste des cellule du jeu
     * @return une liste contenant les cellules du jeu
     */
    public ArrayList<Cellule> donneEtatGrille()
    {
        ArrayList<Cellule> etatGrille = new ArrayList<Cellule>();
        for (int x = 0; x < this.tailleGrille; x++)
        {
            for (int y = 0; y < this.tailleGrille; y++)
            {
                etatGrille.add(this.grilleJeu[x][y]);
            }
        }
        return etatGrille;
    }

    /**
     * Retourne l'ensemble de cases a decouvrir suite a la selection
     * d'une cellule.
     * @param xCell numero de ligne de la cellule selectionnee
     * @param yCell numero de colonne de la cellule selectionnee
     * @return <code>null</code> si la cellule contient une mine,
     *         la liste des cellules a decouvrir dans le cas contraire.
     */
    public ArrayList<Cellule> joue(int xCell, int yCell)
    {
        // ensemble des cellules a decouvrir
        ArrayList<Cellule> cellulesADecouvrir = new ArrayList<Cellule>();
        // selection de la cellule jouee
        Cellule cible = this.grilleJeu[xCell][yCell];

        // si c'est une mine...
        if (cible.donneLibelle().equals("M"))
        {
            return null;
        }
        else if (cible.aEteJouee()) // la cellule a deja ete jouee
        {
            // on retourne un dictionnaire vide...
            return cellulesADecouvrir;
        }
        else
        {
            // on place la cellule selectionnee dans l'ensemble des cellules a decouvrir
            cellulesADecouvrir.add(cible);
            // on la marque
            cible.marque(true);

            ArrayList<Cellule> cellulesVoisinage = this.donneCellulesVoisinage(cible);
            ArrayList<Cellule> cellulesAExplorer = new ArrayList<Cellule>();
            // selection des cellules non explorees dont la valeur est a 0
            for (Cellule c : cellulesVoisinage)
            {
                if (!c.aEteJouee() && (c.donneValeur() == 0))
                {
                    cellulesAExplorer.add(c);
                }
            }
            // exploration de ces cellules
            for (ListIterator<Cellule> it = cellulesAExplorer.listIterator(); it.hasNext();)
            {
                Cellule cellExp = it.next();
                // on supprime les cellules au fur et a mesure de leur exploration
                it.remove();
                // on explore les cellules en jouant (appel recursif)
                ArrayList<Cellule> cellules = this.joue(cellExp.donneLigne(), cellExp.donneColonne());
                // si la cellule a explorer contient une mine, on ne fait rien
                if (cellules != null)
                {
                    // on ajoute les nouvelles cellules obtenues :
                    // - a la liste des cellules a explorer
                    // - a la liste des cellules a decouvrir
                    for (Cellule nouvelleCellule : cellules)
                    {
                        // pour l'exploration, meme regle que precedemment
                        if (!nouvelleCellule.aEteJouee() && (nouvelleCellule.donneValeur() == 0))
                        {
                            it.add(nouvelleCellule);
                        }
                        cellulesVoisinage.add(nouvelleCellule);
                    }
                }
            }
            // remplissage de la liste avec les cellules explorees
            for (Cellule cad : cellulesVoisinage)
            {
                cad.marque(true);
                cellulesADecouvrir.add(cad);
            }
        }
        return cellulesADecouvrir;
    }

    public String toString()
    {
        String s = "";
        for (Cellule[] ct : this.grilleJeu)
        {
            for (Cellule c : ct)
            {
                s += "(" + c.toString() + ")";
            }
            s += "\n";
        }
        return s;
    }

    private int calculValeurCellule(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < this.tailleGrille && y < this.tailleGrille)
        {
            if (this.grilleJeu[x][y].donneLibelle().equals("M"))
            {
                return 1;
            }
        }
        return 0;
    }

    private ArrayList<Cellule> donneCellulesVoisinage(Cellule c)
    {
        ArrayList<Cellule> voisinage = new ArrayList<Cellule>();
        int x = c.donneLigne();
        int y = c.donneColonne();
        // exploration du voisinage de la cellule
        for (int dx = -1; dx <= 1; dx++)
        {
            for (int dy = -1; dy <= 1; dy++)
            {
                // verification de la position dans la grille
                if ((x + dx) >= 0 && (y + dy) >= 0
                        && (x + dx) < this.tailleGrille && (y + dy) < this.tailleGrille)
                {
                    // on en traite pas la cellule dont on veut determiner le voisinage
                    if (dx != 0 || dy != 0)
                    {
                        // on ne prend que les cellules dont qui ne contiennent pas de mines
                        if (!this.grilleJeu[x + dx][y + dy].donneLibelle().equals("M"))
                        {
                            voisinage.add(this.grilleJeu[x + dx][y + dy]);
                        }
                    }
                }
            }
        }
        return voisinage;
    }

}
