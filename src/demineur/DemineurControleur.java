package demineur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Classe representant la logique de fonctionnement associee a l'interface
 * graphique. Elle joue le role de mediateur entre le jeu et
 * l'interface graphique.
 *
 */
public class DemineurControleur implements ActionListener
{
    private DemineurJeu jeu;
    private DemineurGraphique interfaceGraphique;

    /**
     * Construit un controleur en lui fournissant une reference au jeu.
     * @param c le convertisseur a associer
     */
    public DemineurControleur(DemineurJeu jeu)
    {
        this.jeu = jeu;
    }

    /**
     * Definit la logique de fonctionnement de l'interface graphique.
     */
    public void testCaseJouee(int xCase, int yCase)
    {
        // permet de recuperer le resutat d'un tour de jeu
        ArrayList<Cellule> result = this.jeu.joue(xCase, yCase);
        // si la liste est vide cela signifie que :
        // - soit il y avait une mine alors le jeu est fini
        // - soit c'etait la derniere case non decouverte, le joueur
        //   a gagne et le jeu est fini.
        if (result == null)
        {
            this.interfaceGraphique.afficheCasesDecouvertes(this.jeu.donneEtatGrille());
            if (!this.jeu.partieTerminee())
            {
                this.interfaceGraphique.afficheEchecEtFin(xCase, yCase);
            }
            else
            {
                this.interfaceGraphique.afficheGagneEtFin();
            }
        }
        // sinon on affiche les cases decouvertes et on verifie si
        // la partie est terminee (restent uniquement des mines non decouvertes)
        else
        {
            // on affiche les cases decouvertes
            this.interfaceGraphique.afficheCasesDecouvertes(result);
            // puis on teste si la partie est terminee
            if (this.jeu.partieTerminee())
            {
                // si fin alors on affiche toute la grille
                this.interfaceGraphique.afficheCasesDecouvertes(this.jeu.donneEtatGrille());
                this.interfaceGraphique.afficheGagneEtFin();
            }
        }
    }

    /**
     * Associe une interface graphique au controleur.
     * @param ihm l'interface graphique
     */
    public void associeInterfaceGraphique(DemineurGraphique ihm)
    {
        this.interfaceGraphique = ihm;
    }

    /**
     * Methode appelee lors d'un clic sur un des boutons de la grille de jeu.
     *
     * @param e
     */
    public void actionPerformed(ActionEvent e)
    {
        // recuperation des coordonnees de la cellule avec getSource()
        // puis appel de la methode testCaseJouee
        CelluleGraphique cg = (CelluleGraphique)e.getSource();
        testCaseJouee(cg.donneLigne(), cg.donneColonne());
    }

}
