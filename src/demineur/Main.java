package demineur;

/**
 * Represente la classe principale de l'application qui contient la methode
 * <code>main</code>.
 */
public class Main
{
    /**
     * Lance l'application en creant l'interface graphique, le controleur
     * et le jeu. Puis affiche l'interface graphique une fois que
     * les references entre les trois objets (interface, controleur,
     * jeu) ont ete initialisees.
     */
    public static void main(String[] args)
    {
        DemineurJeu jeu = new DemineurJeu(10, 0.15D);
        DemineurControleur controleur = new DemineurControleur(jeu);
        DemineurGraphique interfaceGraphique = new DemineurGraphique(controleur, jeu.donneTailleJeu());
        controleur.associeInterfaceGraphique(interfaceGraphique);
        // a appeler en dernier de preference...
        interfaceGraphique.afficheInterface();
    }
}
